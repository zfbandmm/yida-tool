var progress = require('progress-stream');
var spawn = require("child_process").spawn;
var numeral = require('numeral');
var request = require('https');
var fs = require("fs");
var log = require('single-line-log').stdout;
const host = "172.20.51.87";
const port = "8998";
const unzipper = require("unzipper");
const utils = require("./utils").utils
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"//不校验证书
function downloadTemplate(options, callback) {
    var str = progress({
        drain: true,
        time: 100,
        speed: 20
    });
    str.on('progress', function(progress) {
        log('Running: '.magenta + numeral(progress.runtime).format('00:00:00').cyan + ' ('.cyan +numeral(progress.transferred).format('0 b')+')\n'.cyan +
            'Left:    '.magenta + numeral(progress.eta).format('00:00:00').cyan + ' ('.cyan +numeral(progress.remaining).format('0 b')+')\n'.cyan +
            numeral(progress.speed).format('0.00b').magenta + '/s '.magenta + Math.round(progress.percentage) +'%');
    });
    request.request(options, function(response) {
        response.pipe(str).pipe(fs.createWriteStream(options.zipFileName));
        response.on('end', function () {
            callback && callback();
        });
    }).end();
}
function createComponentDir(dirName) {
    fs.mkdirSync(dirName);//创建文件夹
}
function replaceKey(userInput) {
    let componentName = userInput.componentName;
    let componentType = userInput.componentType;
    let componentName_CN = userInput.componentName_CN;
    let codeTemplate = fs.readFileSync(`./${componentName}/index.tsx`).toString();
    codeTemplate = utils.stringReplaceAll(codeTemplate, "${componentName}", componentName.charAt(0).toUpperCase() + componentName.slice(1));
    codeTemplate = utils.stringReplaceAll(codeTemplate, "${componentType}", componentType);    
    codeTemplate = utils.stringReplaceAll(codeTemplate, "${componentName_CN}", componentName_CN);
    fs.writeFileSync(`./${componentName}/index.tsx`, codeTemplate);
}
function unzipTemplate(userInput, zipFile, toDir, isReplaceKey) {
    setTimeout(()=> {
        fs.createReadStream(zipFile)
        .pipe(unzipper.Extract({ path: `${toDir}` }))
        .on('close', () => {
            console.log("\n ✔  ".green + `${toDir}的模板代码已生成`.green); 
            fs.unlinkSync(`./${zipFile}`);
            isReplaceKey && replaceKey(userInput);
        });
    }, 1000)
}
function execCmd(command, callback) {
    var workerProcess = spawn(command, {
        cwd: './',
        shell: true,
        detached: false,
        env : { FORCE_COLOR: true },
        stdio: ["inherit"]
    });
    workerProcess.stdout.on('data', function (data) {
        console.log(data.toString());
    });
   
    workerProcess.stderr.on('data', function (data) {
        console.log(data.toString());
    });
   
    workerProcess.on('close', function (code) {
        callback && callback();
    });
}
exports.yidaCli = {
    requestTemplate(userInput) {
        downloadTemplate({
            method: 'GET',
            host: host,
            port: port,
            zipFileName: "template.zip",
            path: "/template.zip",
            headers: {
                'user-agent': 'app test'
            }
        }, function() {
            createComponentDir(userInput.componentName);
            unzipTemplate(userInput, 'template.zip', userInput.componentName, true);
        });
    },
    initDevelopeEvn() {
        // const cmd = `git clone -b feature_external_material https://gitlab.sit.com.cn/sailing_apaas/sap-web.git && cd sap-web && git submodule init && git submodule update && cd packages && git checkout -b feature_external_material && git pull origin feature_external_material`
        // execCmd(cmd, function() {
        //     console.log("\n ✔  ".green + "初始化环境已完成, 请分别跳转至sap-web, packages目录手动安装依赖".green);
        // });
        downloadTemplate({
            method: 'GET',
            host: host,
            port: port,
            zipFileName: "sap-web.zip",
            path: "/sap-web.zip",
            headers: {
                'user-agent': 'app test'
            }
        }, function() {
            unzipTemplate({}, 'sap-web.zip', './', false);
            console.log("\n ✔  ".green + "初始化环境已完成, 请分别跳转至sap-web, packages目录手动安装依赖".green);
        });
    }
}