exports.utils = {
    stringReplaceAll(str, AFindText, ARepText){
        var raRegExp = new RegExp(AFindText.replace(/([\(\)\[\]\{\}\^\$\+\-\*\?\.\"\'\|\/\\])/g,"\\$1"),"ig");
        return str.replace(raRegExp,ARepText);
    }
} 