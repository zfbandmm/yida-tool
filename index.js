var program = require("commander");
var fs = require("fs");
var colors = require("colors");
var readline = require("readline");
var yidaCli = require("./yidaCli").yidaCli;
global.config = {};
program
    .version("0.0.1")
    .option("-c,--create", "创建一个物料")
    .option("-r,--remove <string>", "删除一个物料")
    .option("-e,--environment", "生成一个开发环境")
    .parse(process.argv);
var inputParams = process.argv;
if(program.create) {//创建物料
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    let userInput = {};
    function interactWithUser() {
        rl.question('请输入组件英文名(驼峰格式)：', (name) => {
            if(fs.existsSync(name)) {
                console.log("此文件已存在, 请重新输入".red);
                interactWithUser();
                return;
            }
            userInput["componentName"] = name;
            rl.question('请输入组件中文名：', (name) => {
                userInput["componentName_CN"] = name;
                function inputComponentType() {
                    rl.question('请输入组件分类("base" | "form" | "senior" | "customer")：', (name) => {
                        if(name != "base" && name != "form" && name != "senior" && name != "customer") {
                            console.log("组件分类必须为: base, form, senior, customer 中的一种".red);
                            inputComponentType();
                            return;
                        }
                        userInput["componentType"] = name;
                        rl.close();
                        yidaCli.requestTemplate(userInput);
                    });
                }
                inputComponentType();
            });   
        });
    }
    interactWithUser();
}
if(program.remove) {//删除物料
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    let dirName = inputParams[inputParams.length - 1];
    function deleteFolderRecursive(path) {
        if( fs.existsSync(path) ) {
            fs.readdirSync(path).forEach(function(file) {
                var curPath = path + "/" + file;
                if(fs.statSync(curPath).isDirectory()) { // recurse
                    deleteFolderRecursive(curPath);
                } else {
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(path);
        }
    };
    rl.question(`确认删除${dirName} (yes/no)`, (name) => {
        if(name == "yes") {
            deleteFolderRecursive(dirName);
            console.log(`✔  ${dirName}已被成功删除`.green);
        }
        rl.close();
    });
}
if(program.environment) {//生成一个物料开发的环境
    yidaCli.initDevelopeEvn()
}



